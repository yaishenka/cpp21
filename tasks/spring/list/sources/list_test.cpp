// [genius_naming][nolint]

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "list.hpp"
#include "utils.hpp"
#include "memory_utils.hpp"

size_t MemoryManager::type_new_allocated = 0;
size_t MemoryManager::type_new_deleted = 0;
size_t MemoryManager::allocator_allocated = 0;
size_t MemoryManager::allocator_deallocated = 0;
size_t MemoryManager::allocator_constructed = 0;
size_t MemoryManager::allocator_destroyed = 0;

template <typename T, bool PropagateOnConstruct, bool PropagateOnAssign>
size_t WhimsicalAllocator<T, PropagateOnConstruct, PropagateOnAssign>::counter = 0;

size_t Accountant::ctor_calls = 0;
size_t Accountant::dtor_calls = 0;

bool ThrowingAccountant::need_throw = false;

void SetupTest() {
  MemoryManager::type_new_allocated = 0;
  MemoryManager::type_new_deleted = 0;
  MemoryManager::allocator_allocated = 0;
  MemoryManager::allocator_deallocated = 0;
  MemoryManager::allocator_constructed = 0;
  MemoryManager::allocator_destroyed = 0;
}

TEST_CASE("DefaultConstruct", "[List: Construct]") {
  SetupTest();
  {
    List<TypeWithFancyNewDeleteOperators> l;
    // NO LINT
    REQUIRE(l.size() == 0);
    REQUIRE(l.empty());
    REQUIRE(MemoryManager::type_new_allocated == 0);
  }
  REQUIRE(MemoryManager::type_new_deleted == 0);
}

TEST_CASE("List:: List(size_t count)", "[List: Construct]") {
  SetupTest();
  {
    constexpr size_t kSize = 10;
    List<TypeWithFancyNewDeleteOperators> l(kSize);
    REQUIRE(l.size() == kSize);
    REQUIRE(MemoryManager::type_new_allocated == 0);
  }
  REQUIRE(MemoryManager::type_new_deleted == 0);
}

TEST_CASE("List(size_t count, Allocator alloc)", "[List: Construct]") {
  SetupTest();
  AllocatorWithCount<TypeWithFancyNewDeleteOperators> alloc;
  constexpr size_t kSize = 10;
  {
    List<TypeWithFancyNewDeleteOperators,
         AllocatorWithCount<TypeWithFancyNewDeleteOperators>> l(kSize, alloc);
    REQUIRE(l.size() == kSize);
    REQUIRE(MemoryManager::type_new_allocated == 0);
    REQUIRE(MemoryManager::type_new_deleted == 0);
    REQUIRE(MemoryManager::allocator_allocated != 0);
    REQUIRE(MemoryManager::allocator_constructed == kSize);
    REQUIRE(alloc.allocator_allocated == 0);
    REQUIRE(alloc.allocator_constructed == 0);
  }
  REQUIRE(MemoryManager::allocator_deallocated != 0);
  REQUIRE(MemoryManager::allocator_destroyed == kSize);
  REQUIRE(alloc.allocator_destroyed == 0);
  REQUIRE(alloc.allocator_deallocated == 0);
}

TEST_CASE("List(size_t count, const T& value, Allocator alloc)", "[List: Construct]") {
  SetupTest();
  constexpr size_t kSize = 10;
  AllocatorWithCount<TypeWithFancyNewDeleteOperators> alloc;
  TypeWithFancyNewDeleteOperators default_value(1);
  List<TypeWithFancyNewDeleteOperators, AllocatorWithCount<TypeWithFancyNewDeleteOperators>> l(kSize, default_value, alloc);
  REQUIRE(l.size() == kSize);
  REQUIRE(MemoryManager::type_new_allocated == 0);
  REQUIRE(MemoryManager::type_new_deleted == 0);
  REQUIRE(MemoryManager::allocator_allocated != 0);
  REQUIRE(MemoryManager::allocator_constructed == kSize);
  REQUIRE(alloc.allocator_allocated == 0);
  REQUIRE(alloc.allocator_constructed == 0);

  for (auto it = l.begin(); it != l.end(); ++it) {
    REQUIRE(it->value == default_value.value);
  }
}

TEST_CASE("List(std::initializer_list<T> init, const Allocator& alloc = Allocator())", "[List: Construct]") {
  SetupTest();
  AllocatorWithCount<int> alloc;
  List<int, AllocatorWithCount<int>> l({1, 2, 3, 4, 5}, alloc);
  REQUIRE(l.size() == 5);
  REQUIRE(MemoryManager::allocator_allocated != 0);
  REQUIRE(MemoryManager::allocator_constructed == 5);
  REQUIRE(alloc.allocator_allocated == 0);
  REQUIRE(alloc.allocator_constructed == 0);
}

TEST_CASE("List(const List& other)", "[List: Construct]") {
  SetupTest();
  const size_t size = 5;
  List<TypeWithCounts, AllocatorWithCount<TypeWithCounts>> l1 = {1, 2, 3, 4, 5};
  List<TypeWithCounts, AllocatorWithCount<TypeWithCounts>> l2(l1);

  REQUIRE(l2.size() == size);
  REQUIRE(MemoryManager::allocator_allocated != 0);
  REQUIRE(MemoryManager::allocator_constructed == size * 2);
  REQUIRE(AreListsEqual(l1, l2));
  for (auto& value : l1) {
    REQUIRE(*value.copy_c == 2);
  }
}

TEST_CASE("OnlyMovable", "[List: Construct]") {
  SetupTest();
  List<OnlyMovable> list;
  // NO LINT
  REQUIRE(list.size() == 0);
  list.emplace_back(0);
  REQUIRE(list.size() == 1);
  OnlyMovable om(0);
  list.push_back(std::move(om));
  REQUIRE(list.size() == 2);
}

TEST_CASE("List(List&& other)", "[List: Construct]") {
  SetupTest();
  const size_t size = 5;
  List<TypeWithCounts, AllocatorWithCount<TypeWithCounts>> l1 = {1, 2, 3, 4, 5};
  List<TypeWithCounts, AllocatorWithCount<TypeWithCounts>> l2(std::move(l1));

  // NO LINT
  REQUIRE(l1.size() == 0);
  REQUIRE(l2.size() == size);
  REQUIRE(MemoryManager::allocator_allocated != 0);
  REQUIRE(MemoryManager::allocator_constructed == size);
  for (auto& value : l1) {
    REQUIRE(*value.copy_c == 1);
    REQUIRE(*value.move_c == 1);
  }
}

TEST_CASE("List& operator=(const List& other)", "[List: AssOperator]") {
  SetupTest();
  const size_t size = 5;
  List<TypeWithCounts> l1 = {1, 2, 3, 4, 5};
  List<TypeWithCounts> l2;
  l2 = l1;
  REQUIRE(l2.size() == size);
  REQUIRE(AreListsEqual(l1, l2));
  for (auto& value : l1) {
    REQUIRE(*value.copy_c == 2);
  }
}

TEST_CASE("Basic func", "[List: basic funct]") {
  SetupTest();
  List<int> lst;

  // NO LINT
  REQUIRE(lst.size() == 0);

  lst.push_back(3);
  lst.push_back(4);
  lst.push_front(2);
  lst.push_back(5);
  lst.push_front(1);

  std::reverse(lst.begin(), lst.end());

  REQUIRE(lst.size() == 5);

  std::string s;
  for (int x: lst) {
    s += std::to_string(x);
  }
  REQUIRE(s == "54321");
}

TEST_CASE("PropagateOnConstruct and PropagateOnAssign", "[List: TestWhimsicalAllocator]") {
  SetupTest();
  List<int, WhimsicalAllocator<int, true, true>> lst;

  lst.push_back(1);
  lst.push_back(2);

  auto copy = lst;
  assert(copy.get_allocator() != lst.get_allocator());

  lst = copy;
  assert(copy.get_allocator() == lst.get_allocator());
}

TEST_CASE("not PropagateOnConstruct and not PropagateOnAssign", "[List: TestWhimsicalAllocator]") {
  SetupTest();
  List<int, WhimsicalAllocator<int, false, false>> lst;

  lst.push_back(1);
  lst.push_back(2);

  auto copy = lst;
  assert(copy.get_allocator() == lst.get_allocator());

  lst = copy;
  assert(copy.get_allocator() == lst.get_allocator());
}

TEST_CASE("PropagateOnConstruct and not PropagateOnAssign", "[List: TestWhimsicalAllocator]") {
  SetupTest();
  List<int, WhimsicalAllocator<int, true, false>> lst;

  lst.push_back(1);
  lst.push_back(2);

  auto copy = lst;
  assert(copy.get_allocator() != lst.get_allocator());

  lst = copy;
  assert(copy.get_allocator() != lst.get_allocator());
}

TEST_CASE("TestAccountant", "[List: TestAccountant]") {
  Accountant::reset();
  {
    List<Accountant> lst(5);
    REQUIRE(lst.size() == 5);
    REQUIRE(Accountant::ctor_calls == 5);

    List<Accountant> another = lst;
    REQUIRE(another.size() == 5);
    REQUIRE(Accountant::ctor_calls == 10);
    REQUIRE(Accountant::dtor_calls == 0);

    another.pop_back();
    another.pop_front();
    REQUIRE(Accountant::dtor_calls == 2);

    lst = another; // dtor_calls += 5, ctor_calls += 3
    REQUIRE(another.size() == 3);
    REQUIRE(lst.size() == 3);

    REQUIRE(Accountant::ctor_calls == 13);
    REQUIRE(Accountant::dtor_calls == 7);
  } // dtor_calls += 6

  REQUIRE(Accountant::ctor_calls == 13);
  REQUIRE(Accountant::dtor_calls == 13);
}

TEST_CASE("Test exception safety", "[List: exception safety]") {
  Accountant::reset();

  ThrowingAccountant::need_throw = true;

  try {
    List<ThrowingAccountant> lst(8);
  } catch (...) {
    REQUIRE(Accountant::ctor_calls == 4);
    REQUIRE(Accountant::dtor_calls == 4);
  }

  ThrowingAccountant::need_throw = false;
  List<ThrowingAccountant> lst(8);

  List<ThrowingAccountant> lst2;
  for (int i = 0; i < 13; ++i) {
    lst2.push_back(i);
  }

  Accountant::reset();
  ThrowingAccountant::need_throw = true;

  try {
    auto lst3 = lst2;
  } catch (...) {
    REQUIRE(Accountant::ctor_calls == 4);
    REQUIRE(Accountant::dtor_calls == 4);
  }

  Accountant::reset();

  try {
    lst = lst2;
  } catch (...) {
    REQUIRE(Accountant::ctor_calls == 4);
    REQUIRE(Accountant::dtor_calls == 4);

    // Actually it may not be 8 (although de facto it is), but the only thing we can demand here
    // is the abscence of memory leaks
    //
    //assert(lst.size() == 8);
  }
}

TEST_CASE("Move", "[List: emplace_back/push_back(T&&)]") {
  TypeWithCounts t(0);
  List<TypeWithCounts> l;
  l.emplace_back(0);
  REQUIRE(*l.begin()->int_c == 1);
  REQUIRE(*l.begin()->move_c == 0);
  REQUIRE(*l.begin()->copy_c == 0);

  l.push_front(std::move(t));
  REQUIRE(*l.begin()->move_c == 1);
  REQUIRE(*l.begin()->copy_c == 0);
}
