# Лекция 16

## Вывод типов

### Ключевое слово auto

В c++ есть ключевое слово auto, которое позволяет в некоторых местах не писать тип. Наша задача разобраться как это работает

Зачем это вообще нужно: 

```c++
int main() {
  std::map<std::string, std::int> m;

  for (std::map<std::string, std::int>::iterator it = m.begin(); it != m.end(); ++it) {
    ...
  }
}
```

VS

```c++
int main() {
  std::map<std::string, std::int> m;

  for (auto it = m.begin(); it != m.end(); ++it) {
    ...
  }
}
```

Еще пример:

```c++
int main() {
  std::map<std::string, std::int> m;

  for (const std::pair<std::string, int>& item : m) {
    ...
  }
}
```

item здесь будут копироваться, потому что внутри контейнера map лежат пары вида std::pair\<const std::string, int\>. Соответственно в этом случае тоже хорошо бы использовать auto

Еще одна причина использовать auto: типы в коде могут поменяться и без auto это превратится в огромный рефакторинг

Auto работает так же как вывод шаблонного типа. Auto понятное дело можно комбинировать со ссылками и константностью:

```c++
int main() {
  std::map<std::string, std::int> m;

  for (const auto& item : m) {
    ...
  }
}
```

***Note*** с auto как и с T работает отбрасывание ссылок, то есть при auto x = "чему-то со ссылкой" ссылка отбросится. Например: 

```c++
int& foo(int& x) {return x; }

int main() {
  int x = 10;
  auto y = foo(x);
  ++y;
}
```

Рассмотрим такой код: 

```c++
int& foo(int& x) {return x; }

int main() {
  int x = 10;
  auto&& y = foo(x);
  ++y;
}
```

И тут auto&& работает как универсальная ссылка, то есть тут работают все те же правила.


### Ключевое слово decltype

decltype это такая метафункция, которая в compile-time возвращает тип выражения

На decltype(obj) можно так же навешивать различные модификаторы типов (прям как с шаблонами)

decltype так же можно брать от выражений

```c++
int x;
decltype(x++) u = x;
```

***Note*** x не увеличится!

Правило работы с выражениями такое:

Если expr это не идентификатор, то
 
+ Если expr это prvalue типа T, то decltype(expr) = T  (decltype(x++))
+ Если expr это lvalue типа T, то decltype(expr) = T& (decltype(++x))
+ Если expr это xvalue типа T, то decltype(expr) = T&& (decltype(std::move(x)))

Интересный пример 

```c++
decltype(throw 1)* p = nullptr; // void* p = nullptr;
```

### Вывод типа для возвращаемого значения функций 

auto можно использовать при объявлении функции:

```c++
auto f(int& x) {
  return x;
}
```

Пример сложней:

```c++
auto f(int& x) {
  if (x > 0) {
    return x;
  }

  return 0.5;
}
```

Тут мы заставили компилятор в рантайме определять тип, что плохо, поэтому CE


Посмотрим на следующую проблему:

```c++
template <typename T>
... g(const T& x) {
  return some_func(x)
}
```

Хотим всегда возвращать тип такой, какой вернет some_func, но допустим что some_func умеет возвращать ссылку или не ссылку для некоторых типов. Поэтому auto не поможет

Написать вот так:

```c++
template <typename T>
decltype(some_func(x)) g(const T& x) {
  return some_func(x)
}
```

Мы тоже не можем потому что x еще не объявлен, поэтому в c++ есть еще такой синтаксис:

```c++
template <typename T>
auto g(const T& x) -> decltype(f(x)) {
  return some_func(x)
}
```

Еще пример:

```c++
template <typename Container>
auto get(const Container& c, size_t i) -> decltype(c[i]) {
  return c[i];
}
```

Проблема такая: не всегда обращение к контейнеру по индексу возвращает ссылку (вектор булов), поэтому приходится вот так извращаться с возвращаемым типом

Начиная с c++14 этот синтаксис упростили

```c++
template <typename Container>
decltype(auto) get(const Container& c, size_t i) {
  return c[i];
}
```

По сути это заставит компилятор выводить тип не по правилам auto а по правилам decltype

***Note*** decltype(auto) можно так же использовать в других выражениях (при инициализации например)

### Structured bindings (since c++17)

```c++
int main() {
  std::pair<int, std::string> p(5, "abc");
  auto [a, b] = p;
  std::cout << a << " " << b << std::endl;
}
```

В этом контексте на auto опять же таки можно навешивать украшатели типа

Это еще работает для std::tuple, работает для сишного массива, работает даже для data members!

Более глубокое понимание https://devblogs.microsoft.com/oldnewthing/20201015-00/?p=104369


## Умные указатели


Умные указатели - указатели которые следят за освобождением ресурса. Построены на идиоме RAII. Вспомним первую проблему где нам они понадобились:

```c++
void f(int x) {
  int* p = new int(1000);
  if (x == 0) {
    throw 1;
  }

  delete p;
}
```

Хотелось бы, чтобы p в деструкторе сам очищал память.

Начнем с более простого умного указателя

### unique_ptr

Unique_ptr - простейший умный указатель. Он запрещает себя копировать и удаляет ресурс в деструкторе. Простота именно в запрете на копирование (реализовать shared_ptr, где несколько объектов владеют ресурсом куда сложней)

Реализация unique_ptr супер простая, приводить ее здесь не будем. Важно реализовать noexcept move-конструктор и move-оператор присваивания, не забыть про стрелочку и звездочку

Еще у умных указателей есть метод .get() который возвращает сырой указатель

Еще из приколов: вот такой код не будет нормально работать

```c++
std::unique_ptr<int> p(new int[5]);
```

Потому что в деструкторе будет вызван delete p. Для массивов существует отдельная специализация 

```c++
std::uniqe_ptr<int[]>p(new int[5]);
```

***Note*** Начиная с c++17 для специализации с массивами есть квадратные скобки

### shared_ptr

Тут много букав про то как пользоваться shared_ptr. Приводить эти букавы не будем :) 

Давайте попробуем лучше накидать реализацию

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr) {}
 private:
  T* ptr;
};
```

Заготовка есть. Давайте разбираться со счетчиком (shared_ptr должен понимать количество владельцев объектом)

Первая версия:

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(1) {}
 private:
  T* ptr;
  size_t count;
};
```
Это очевидно работать не будет, например при копировании нам нужно обновить счетчик у всех инстансов

Следующий вариант:

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(1) {}
 private:
  T* ptr;
  static size_t count;
};
```

Теперь у нас счетчик общий вообще для всего класса shared_ptr\<T\>. Не подойдет!

Лучшее решение:

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(new int(1)) {}
 private:
  T* ptr;
  size_t* count;
};
```

***Note*** невероятно душное замечание: конечно же нам нужно уметь принимать кастомный аллокатор для выделения этого счетчика. Кстати shared_ptr еще умеет принимать кастомный Deleter для удаления ресурса в деструкторе

Давайте попробуем написать деструктор

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(new int(1)) {}
  ~shared_ptr() {
    if (count == nullptr) {
      return;
    }

    --*count;

    if (*count == 0) {
      delete ptr;
      delete count;
    }
  }
 private:
  T* ptr = nullptr;
  size_t* count = nullptr;
};
```

Про остальные методы shared_ptr проще почитать на цппреф

### std::make_shared/unique

#### Общая идея

Давайте посмотрим на следующий код:

```c++
int main() {
  int* p = new int(5);
  std::shared_ptr<int> sp(p);
}
```

Так делать не надо. Ну то есть так конечно все будет работать, но это позволяет вам сделать вот так:


```c++
int main() {
  int* p = new int(5);
  std::shared_ptr<int> sp1(p);
  std::shared_ptr<int> sp2(p);
}
```
А это уже UB.

Вот такой код тоже лучше не писать (ничего плохого в нем нет, но кодстайл так себе):

```c++
int main() {
  std::shared_ptr<int> sp(new int(5));
}
```

Интересный пример:

```c++
int g(const std::shared_ptr<int>& p, int x) {}

int f(int x) {
  if (x == 0) {
    throw 1;
  }

  return x + 1;
}

int main() {
  g(shared_ptr<int>(new int(10)), f(0));
}
```

Теоритически компилятор может сначала сделать new int(10) а потом f(0) (до c++17)

Стл предоставляет интерфейсы для создания умных указателей без обращения к new

#### Реализация std::make_unique

```c++
template <typename T, typename... Args>
uniqe_ptr<T> make_unique(Args&&... args) {
  return uniqe_ptr<T>(new T(std::forward<Args>(args)...));
}

int main() {
  auto p = std::make_unique<int> p(5);
}
```

#### Реализация std::make_shared

Тут все интересней. Зачем нам отдельно делать new для T и new для счетчика, если можно сделать один new. Давайте немного изменим наш класс shared_ptr

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(new int(1)) {}
  ~shared_ptr() {
    if (control_block_ == nullptr) {
      return;
    }

    --control_block_->count;

    if (control_block_->count == 0) {
      delete control_block_;
    }
  }
 private:
  template <typename U>
  struct ControlBlock {
    T object;
    size_t count;
  }

  template <typename U, typename... Args>
  friend shared_ptr<U> make_shared(Args&&... args);

  shared_ptr(ControlBlock<T>* ptr): control_block_(control_block_) {}

  ControlBlock<T>* control_block_;
};

template <typename T, typename... Args>
shared_ptr<T> make_shared(Args&&... args) {
  auto p = new ControlBlock(1, std::forward<Args>(args)...);
  return shared_ptr<T>(p);
}
```
