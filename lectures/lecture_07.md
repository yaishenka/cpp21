# Лекция 7

## Продолжим тему шаблонов

### Алиасы

Начиная с C++14 эти метафункции немного обновили и добавили такого рода функционал:

```c++
using remove_const_t = typename remove_const<T>::type
```

Это немного упращает код, так как не надо обращаться к типу type, а можно просто писать remove_const_t\<T\>

А начиная с C++17 добавили еще такое (появились шаблонные константы):

```c++
template <typename U, typename V>
const bool is_same_v = is_same<U, V>::value;
```

### Variadic templates (since c++11)

Допустим хотим написать функцию print для вывода в поток произвольного количества аргументов.

```c++
#include <iostream>

int main() {
    print(1, 2, 3, "abc", 5.0);
}
```

<details> 
  <summary>А как это сделать в C?</summary>
   Ответ: функция с переменным числом аргументов vastart, vaend и тд. 
</details>

Как это сделать? Начнем с основного синтаксиса и будем дописывать нашу функцию:

```c++
template <typename... Args>
void print(Args... args) {

}
```

<details> 
  <summary>какая первая проблема?</summary>
   Ответ: будем копировать аргументы, надо принимать по константной ссылке 
</details>

```c++
template <typename... Args>
void print(const Args&... args) {

}
```

Как видим модификаторы навешиваются точно так же как в обычных шаблонах.

Как же нам обработать эти аргументы? Давайте "откусим" голову у списка:

```c++
template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail) {
    std::cout << head << std::endl;
    print(tail...)
}
```

Синтаксис неочевидный конечно, но надо привыкать.

Здесь с tail по сути ничего нельзя делать (кроме того чтобы написать tail...), это просто пакет из объектов разного типа.

<details> 
  <summary>Почему такая функция не будет работать?</summary>
   Ответ: tail может быть пустым
</details>

Давайте перепишем нашу функцию, чтобы исправить этот баг

```c++
template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail) {
    std::cout << head << std::endl;
    print(tail...)
}

void print() {}

```

Теперь у нас есть перегрузка от 0 аргументов и все хорошо... Или нет??

<details> 
  <summary>Почему все сломается?</summary>
   Ответ: определили print() ниже чем шаблонную версию и шаблонная версия не видит такой перегрузки
</details>

<details> 
  <summary>Сколько функций print сгенерировал компилятор?</summary>
   Ответ: 5
</details>

#### Что еще можно делать с пакетом? (tail)

1. Можно написать tail... и распаковать пакет

2. Можно сделать sizeof...(tail) и узнать сколько еще аргументов в пакете осталось

#### Давайте напишем еще один пример

Давайте напишем структуру is_same для нескольких аргументов

```c++
template <typename First, typename Second, typename... Types>
struct is_same_many { // is_homogeneous
    const static bool value = is_same_v<First, Second> && is_same_many<Second, Types...>::value;
};

template <typename First, typename Second>
struct is_same_many { // is_homogeneous
    const static bool value = is_same_v<First, Second>
};
```

### Правила вывода шаблонных типов

Рассмотрим такой код:

```c++
template <typename T>
void f(T x) {

}

int main() {
    int x = 0;
    int& y = x;
    const int& z = y;
}
```

Давайте попробуем понять, какой тип подставится при вызовах функции f. Для начала как это вообще делать?

```c++
template <typename T>
class C {
    C() = delete;
};

template <typename T>
void f(T x) {
    C<T> c;
}

int main() {
    int x = 0;
    int& y = x;
    const int& z = y;
    const int c = 0;
}
```

Т.к. конструктор по умолчанию мы удалили, то мы словим CE и компилятор нам напишет какой тип подставился :)

Попробуем понять какой тип будет при следующих вызовах:


1. f(x) -> T = int

2. f(y) -> T = int => при шаблонной постановке компилятор отбрасывает &. Почему? Потому что это логично... Вы пишите так, как будто хотите, чтобы x принимался по значению, поэтому компилятор убирает &. А еще y это псевдоним для x, поэтому было бы странно если для x и для y были бы разные вызовы

3. f(z) -> T = int, та же логика

4. f(c) -> T = int, просто смиритесь 

А теперь перепишем нашу функцию f:

```c++
template <typename T>
class C {
    C() = delete;
};

template <typename T>
void f(T& x) { // тут добавили &
    C<T> c;
}

int main() {
    int x = 0;
    int& y = x;
    const int& z = y;
    const int c = 0;
}
```

Посмотрим на те же вызовы: 

1. f(x) -> T = int

2. f(y) -> T = int, т.к. T& это int& => T = int

3. f(z) -> T = const int, т.к. T& это const int& => T = const int

4. f(c) -> T = const int

И наконец константная ссылка

```c++
template <typename T>
class C {
    C() = delete;
};

template <typename T>
void f(const T& x) { // тут добавили const
    C<T> c;
}

int main() {
    int x = 0;
    int& y = x;
    const int& z = y;
    const int c = 0;
}
```

1. f(x) -> T = int

2. f(y) -> T = int

3. f(z) -> T = int

4. f(c) -> T = int

Ну и еще один грязный трюк

```c++
template <typename T>
class C {
    C() = delete;
};

template <typename T>
void f(T& x) {
    C<T> c;
}

int main() {
    int x = 0;
    int& y = x;
}
```

f<int&>(y) -> int& => если при шаблонной подстановке происходит навешивание ссылки на ссылку, то компилятор оставляет просто ссылку. Это правило нам еще очень пригодится при изучении move-семантики)

#### reference-wrapper

Допустим у нас есть функция, которая принимает x по значению, а мы очень хотим отдать туда x по ссылке. Для этого существует функция std::ref() в заголовке functional

```c++
template <typename T>
class C {
    C() = delete;
};

template <typename T>
void f(T x) {
    C<T> c;
}

int main() {
    int x = 0;
    f(std::ref(x));
}
```

В этом случае T = std::reference_wrapper\<int\>. Этот класс инкапсулирует логику ссылок, но дает возможность менять владельца. По сути она работет скорее как указатель) 

## Исключения

### Основная идея

Хотим обрабатывать случаи, когда что-то идет не так (деление на 0, сегфолт и тд)

### Как это было сделано в C

```c++
int f(int x, int y, int& error) {
    if (y == 0) {
        error = 0;
        return x;
    }

    error = 1;
    return x / y;
}

int main() {
    int error = 0;

    f(5, 0, error);

    if (error == 1) {
        ...
    } 
}
```

Очевидно, что это очень неудобно, поэтому в языке c++ появился механизм выброса исключений.

### Синтаксис 

Для этого появился специальный оператор throw (это не управляющяя конструкция, а именно оператор)

Синтаксис:


```c++
int f(int x, int y {
    if (y == 0) {
        throw 1;
    }

    return x / y;
}
```

Теперь есть два выхода из функции f: через return и через throw

Бросать просто int это очень плохо, но о нормальных объектах исключений мы поговорим чуть позже.

Как ловить исключение?


```c++
int f(int x, int y) {
    if (y == 0) {
        throw 1;
    }

    return x / y;
}

int main() {
    try {
        f(1, 0);
    } catch (int x) {
        ...
    }
}
```

***Note*** try-catch это уже управляющий конструкция

Ловить можно не только конкретный тип, а любой:

```c++
try {
    f(1, 0);
} catch (...) {
    // error here
}
```


### Разница между исключениями и ошибками

Не любая ошибка является исключением! 

Например, деление на 0 не является исключением.

try-catch может поймать только то, что было брошено с помощью throw!

***Note*** В питоне, например, можно поймать выход за границу массива. Опять жертвуем безопасностью в пользу скорости

Все же в плюсах есть некоторые ошибки в стандартной библиотеке, которые являются исключениями:

 1. dynamic_cast. Бросает он std::bad_cast

 2. оператор new. Бросает std::bad_alloc

 3. метод at в векторе. Бросает std::out_of_range

### Стандартная иерархия исключений

https://en.cppreference.com/w/cpp/error/exception

У каждого исключения можно вызвать метод what() (причем он виртуальный). С помощью него можно узнать что именно случилось


### Подробней про catch

1. В catch запрещены все приведения типов, кроме приведения типов между наследником и родителем и очевидных (int->const int)

2. Если есть несколько catch, то компилятор выберет первый подходящий:

```c++
try {
    throw std::out_of_range("aaaa");
} catch (std::exception& ex){
    std::cout << 1;
} catch (std::out_of_range& ex) {
    std::cout << 2;
} catch (...) {
    std::cout << 3;
}
```

Выведется 1.

3. throw сделанный в catch игнорируется остальными catch и бросается выше по стеку:

```c++
try {
    throw std::out_of_range("aaaa");
} catch (std::exception& ex){
    throw 1;
    std::cout << 1;
} catch (std::out_of_range& ex) {
    std::cout << 2;
} catch (...) {
    std::cout << 3;
}
```

Такой код упадет с непойманной ошибкой.

### Исключения и копирование.

```c++
struct S {
    S() {
        std::cout << "create\n";
    }

    S(const S&) {
        std::cout << "copy\n";

    }

    ~S() {
        std::cout << "destroy\n";
    }
}

void f() {
    S s;
    throw s;
}

int main() {
    try {
        f();   
    } catch (const S& s) {
        std::cout << "caught";
    }
}
```
Выведется: create copy destroy caught destroyed

***Note*** на самом деле будет копирование, а не перемещение, но об этом потом

А теперь вот так:

```c++
struct S {
    S() {
        std::cout << "create\n";
    }

    S(const S&) {
        std::cout << "copy\n";

    }

    ~S() {
        std::cout << "destroy\n";
    }
}

void f() {
    S s;
    throw s;
}

int main() {
    try {
        f();   
    } catch (S s) {
        std::cout << "caught";
    }
}
```

А теперь будет два копирования:


Выведется: create copy destroy copy caught destroyed destroyed

### Посылаем исключение дальше


```c++
int main() {
    try {
        f();   
    } catch (const S& s) {
        std::cout << "caught";
        throw;
    }
}
```

В чем разница со следующим кодом?

```c++
int main() {
    try {
        f();   
    } catch (const S& s) {
        std::cout << "caught";
        throw s;
    }
}
```

Ответ: бросаем тот же объект, а не новый

Это хорошо иллюстрирует этот пример:

```c++
void f() {
    try {
        throw std::out_of_range("aaa");
    } catch (std::exception& ex) {
        throw;
    }
}

int main() {
    try {
        g();
    } catch (std::out_of_range& oor) {
        std::cout << "caught";
    }
}
```

VS

```c++
void f() {
    try {
        throw std::out_of_range("aaa");
    } catch (std::exception& ex) {
        throw ex;
    }
}

int main() {
    try {
        g();
    } catch (std::out_of_range& oor) {
        std::cout << "caught";
    }
}
```

### Исключения в конструкторах и идиома RAII

Посмотрим на следующий код:

```c++
void f() {
    int* p = new int(10);

    throw 1;

    delete p;
}

int main() {
    try {
        f();
    } catch(...) {

    }
}
```

будет утечка памяти. 

Эта проблема приводит нас к следующей проблеме:

```c++
struct S {
    int* p = nullptr;
    S(): p(new int(5)) {
        throw 1;
    }

    ~S() {
        delete p;
    }
}

int main() {
    try {
        S s;
    } catch(...) {

    }
}
```

Деструктор не будет вызван! 

Как решать эту проблему?

1. Перед throw все очистить

2. Использовать умные указатели

Подробно говорить про умные указатели будем потом, но сейчас важно понять идиому вокруг которой они сделаны:

RAII - Resource Acquisition Is Initialization 

Получение ресурса есть инициализация


Напишем заготовку умного указателя:

```c++
template <typename T>
struct SmartPtr {
    T* ptr;
    SmartPtr(T* ptr): ptr(ptr) {}
    ~SmartPtr() {
        delete ptr;
    }
}
```

теперь если мы будем пользоваться таким указателем, то все будет ок.

Но с такими объектами тяжело работать, потому что непонятно как их копировать. Умные указатели и перемещение в плюсах решают эту проблему) 

***Note*** RAII это не только про память, можно придумать кучу ресурсов кроме памяти, с которыми будут те же проблемы.












