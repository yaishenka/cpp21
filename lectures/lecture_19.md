# Лекция 19

## std::function

std::function это такой тип, которому можно присвоить callable объекты

```c++
#include <functional>

struct S {
  bool operator()(int x, int y) const {
    return y < x;
  }
}

bool g(int x, int y) {
  return x == y;
}

int main() {
  std::function<bool>(int, int) f;

  f = [](int x, int y) {return x < y; };
  f(1, 2);

  f = S();
  f(1, 2);

  f = g; // f = &g 
  f(1, 2);
}
```

## Шаблонное метапрограммирование, SFINAE и вычисления в compile-time

RIP XXXTentacion

### Идея SFINAE и простейший пример

SFINAE = substitution failure is not an error

"Неудачная шаблонная подстановка не является ошибкой компиляции"

```c++
template <typename T>
auto f(const T&) {
  std::cout << 1;
  return 0;
}

auto f(...) {
  std::cout << 2;
  return 0;
}
```

f(5) -> 1 (очевидно)

```c++
template <typename T>
auto f(const T&) -> decltype(T().size()) {
  std::cout << 1;
  return 0;
}

auto f(...) {
  std::cout << 2;
  return 0;
}
```

Если сейчас вызвать f от какого-нибудь вектора, то выведется 1 (причем это будет size_t), а если от инта то выведется 2 причем инт. Собственно тут и происходит SFINAE. При подставновке int в первую версию функции возникает ошибка подстановки (нельзя сделать int().size()), но это не ошибка компиляции и компилятор пытается выбрать следующую перегрузку


Это работает только если ошибка возникает именно во время подстановки в сигнатуру функции, а не в тело 

SFINAE позволяет делать нам кучу интересных вещей, давайте с ними разбираться

## std::enable_if использование и реализация 

Очень часто мы хотим выключать какие-то перегрузки в зависимости от типа T. В этом нам поможет enable_if

Пример: хотим включать перегрузку только если T это класс. Воспользуемся std::is_class

```c++
#include <type_traits>

template <typename T, typename = std::enable_if_t<std::is_class_v<std::remove_reference_t<T>>>>
void g(const T&) {
  std::cout << 1;
}

template <typename T, typename = std::enable_if_t<!std::is_class_v<std::remove_reference_t<T>>>>
void g(T&&) {
  std::cout << 2;
}
```

Давайте реализуем

```c++
template <bool B, typename = void>
struct enable_if {};

template <typename T>
struct enable_if<true, T> {
  using type = T;
}

template <bool B, typename T = void>
using enable_if_t = typename enable_if<B,T>::type;
```

### Проверка наличия метода в классе

Давайте напишем мета функцию, которая будет проверять есть ли в классе метод construct с данными аргументами


```c++
template <typename T, typename... Args>
struct has_method_construct {
 public:
  static const bool value = ...;
 private:

}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Нужно написать какой-то метод в приватной части, который будет с использованием SFINAE все определять

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  static auto f(int) -> decltype(T().construct(Args()...));
  static bool f(...);
 public:
  static const bool value = ...;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Есть проблема: мы хотим чтобы возвращаемое значение было bool в идеале, но у нас возвращаемое значение это decltype(T().construct(Args()...)). На помощь нам приходит оператор запятая

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  static auto f(int) -> decltype(T().construct(Args()...), bool());
  static bool f(...);
 public:
  static const bool value = ...;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Теперь нам очень хочется написать так: static const bool value = f(0), но нам же нужно делать проверки в compile-time

Можно написать что эти функции constexpr и тогда компилятор будет обязан посчитать их на этапе компиляции, но давайте попробуем по-другому. Мы умеем сравнивать типы, давайте все таки сделаем разные типы у наших функций

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  static auto f(int) -> decltype(T().construct(Args()...), int());
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f(0)), int>;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Такая реализация не будет работать, когда метода нет. Почему? Потому что T у нас шаблонный параметр класса, а не функции, поэтому SFINAE не отрабатывает

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  template <typename TT, typename... AArgs>
  static auto f(int) -> decltype(TT().construct(AArgs()...), int());

  template <typename...>
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f<T, Args...>(0)), int>;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Это все еще не совсем верная реализация, потому что у типа T как и у типа Args может не быть конструктора по умолчанию. 

(можно проверить написав класс NoDefaultConstruct и принимать его в качестве аргумента в методе construct())

## Проблема конструктора по умолчанию и declaval()

Для типа T мы хотим иметь выражение которое будет иметь тип T, при этом у типа T может отсутствовать конструктор по умолчанию

Для этого существует функция declval. Первое что приходит в голову:

```c++
template <typename T>
T declval() noexcept;
```

Тогда нашу функцию has_method_construct нужно переписать так:

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  template <typename TT, typename... AArgs>
  static auto f(int) -> decltype(declval<TT>().construct(declval<AArgs>()...), int());

  template <typename...>
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f<T, Args...>(0)), int>;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Что написано в теле функции declval? Ничего. Эта функция нужна только для compile-time приколов. 

А еще эта функция на самом деле возвращает T&&

```c++
template <typename T>
T&& declval() noexcept;
```

Это позволяет нам работать с типами у которых нет тела, а так же с шаблонными типами которые не придется инстанцировать (incomplete type)

## is_constructible, is_copy_constructible, ...

is_constructible тривиально получается из has_method_construct

```c++
template <typename T, typename... Args>
struct is_constructible {
 private:
  template <typename TT, typename... AArgs>
  static auto f(int) -> decltype(TT(declval<AArgs>()...), int());

  template <typename...>
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f<T, Args...>(0)), int>;
}

template <typename T, typename... Args>
const bool is_constructible_v = is_constructible<T, Args...>::value
```

Давайте еще напишем is_copy_constructible


```c++
template <typename T>
struct is_copy_constructible {
 private:
  template <typename TT>
  static auto f(int) -> decltype(TT(declval<const TT&>()), int());

  template <typename...>
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f<T>(0)), int>;
}

template <typename T>
const bool is_copy_constructible_v = is_copy_constructible<T>::value
```

На самом деле можно просто написать так: 

```c++
template <typename T>
const bool is_copy_constructible_v = is_constructible<const T&>::value
```









