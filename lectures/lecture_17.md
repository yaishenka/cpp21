# Лекция 17

## Продолжение умных указателей

### Weak_ptr

Давайте посмотрим на следующий код:

```c++
struct S {
	std::shared_ptr<S> ptr;
}

int main() {
	auto s1 = std::make_shared<S>();
	auto s2 = std::make_shared<S>();

	s1.ptr = s2;
	s2.ptr = s1;
}
```

В данном случае получится утечка памяти, потому что shared никогда не удалится. Данная проблема может показаться надуманной, однако возникает очень много ситуаций в реальном мире где эта проблема актуальна (например хранение родителя в дереве, хранение указателя на предыдущий элемент в списке). Из-за этой проблемы сборка мусора это не очень тривиальная задача. Одно из решений этой проблемы это "слабые ссылки". В случае c++ это weak_ptr

Weak ptr хоть и назван как указатель, однако он не ведет себя как таковой. Его нельзя разименовывать! 

Weak ptr умеет делать только две вещи:

1. Отвечать на вопрос "Сколько еще шаредов указывает на объект"

2. По запросу создавать новый шаред на объект


Т.е. weak ptr не владеет объектом и не учавствует в подсчете ссылок.


Такая конструкция решает наши проблемы: просто используем weak_ptr для указания на родителей

Давайте накидаем скелет реализации

```c++
template <typename T>
class weak_ptr {
 public:
  weak_ptr(const shared_ptr<T>& ptr): helper_(ptr.helper_) {}

  bool expired() const {}

  shared_ptr<T> lock() const { return shared_ptr<T>(helper_); }
 private:
  ControlBlock<T>* helper_ = nullptr;
}
```

Вспомним, что наш контрольный блок выглядит так: 

```c++
template <typename U>
struct ControlBlock {
  T object;
  size_t count;
}
```

Очень хочется написать метод expired вот так:

```c++
template <typename T>
class weak_ptr {
 public:
  weak_ptr(const shared_ptr<T>& ptr): helper_(ptr.helper_) {}

  bool expired() const {
    return helper_->count == 0;
  }

  shared_ptr<T> lock() const { return shared_ptr<T>(helper_); }

 private:
  ControlBlock<T>* helper_ = nullptr;
}
```

На самом деле это будет ошибка, потому что при обнулении счетчика наш control_block удаляется в деструкторе shared_ptr

На самом деле нам нужно завести второй счетчик, а так же хранить объект внутри блока через указатель 

```c++
template <typename U>
struct ControlBlock {
  T* object;
  size_t shared_count;
  size_t weak_count;
}
```

Теперь в деструкторе shared_ptr нам нужно удалять объект при обнулении shared_count и удалять весь блок при обнулении weak_count

в weak_ptr в методе expired мы можем смотреть на shared_count, а в деструкторе удалять блок если weak_count становится нулем

### enable_shared_from_this

Проблема следующая: допустим нам изнутри класса нужно вернуть указатель на себя. Мы люди умные и хотим работать с умными указателями, но есть нюанс: просто создать shared_ptr от this мы не можем!

Простое решение проблемы: давайте в полях хранить weak_ptr на себя. Но это неудобно, потому что тогда бы для каждого такого класса пришлось бы заводить новое поле. В СТЛ все придумано за нас! 

Чтобы ваш класс мог выкидывать шаред на себя вам нужно отнаследоваться от класса std::enable_shared_from_this

Здесь мы познакомимся с приемом CRTP (Curiously recursive template pattern)

```c++
struct S: public std::enable_shared_from_this<S> {
  shared_ptr<S> GetPtr() const {
    return shared_from_this();
  }
}
```

Давайте прикинем как он устроен:

```c++
template <typename T>
class enable_shared_from_this {
 protected:
  shared_ptr<T> shared_from_this() const {
    return ptr_.lock();
  }
 private:
  weak_ptr<T> ptr_ = nullptr;
}
```

Вызов shared_from_this из объекта, на который нет shared_ptr до 17х плюсов приводил к UB, после 17х плюсов кидает исключение 


Самое интересное: как эта структура создается? 

Нужно переписать shared_ptr и проверять в нем, что если текущий объект является частным случаем enable_shared_from_this, то нужно еще проинициализировать внутри него эту структуру.


Проверить можно с помощью constexpr if (if который проверяется на этапе компиляции) и метафункции std::is_base_of
