# Лекция 10

## Стандартные контейнеры

### Обзор контейнеров

Контейнеры делятся на следующие две большие группы: последовательные и ассоциативные

### Последовательные контейнеры

#### std::vector

Давайте поймем с какой ассимптотикой работает каждая операция

* index\[\]

Работает за O(1)

* push_back

Работает за O(1) амортизированно (потому что  реалокация)

* find

Отсутствует

* insert по итератору 

Работает за O(n)

* erase по итератору 

Работает за O(n)

* категория итератора 

ContiguousIterator

#### std::deque

* index\[\]

Работает за O(1)

* push_back/front

Работает за O(1) амортизированно (потому что  реалокация)

* find

Отсутствует

* insert по итератору 

Работает за O(n)

* erase по итератору 

Работает за O(n)

* категория итератора 

RandomAccessIterator

#### std::list (forward_list)

* index\[\]

Отсутствует

* push_back (отсутствует в forward_list) /front

Работает за O(1)

* find

Отсутствует

* insert по итератору 

Работает за O(1)

* erase по итератору 

Работает за O(1)

* категория итератора 

В двусвязном списке BidirectionalIterator

В односвязаном ForwardIterator

### Ассоциативные контейнеры

#### map/set (к/ч)
 
* index\[\]

O(log n)

* push_back

Отсутствует

* find

O(log n)

* insert по итератору 

O(log n)

* erase по итератору 

O(log n)

* категория итератора 

BidirectionalIterator

#### unordered_map/unordered_set
 
* index\[\]

O(1) (не совсем честно)

* push_back

Отсутствует

* find

O(1) (не совсем честно)

* insert по итератору 

O(1) (не совсем честно)

* erase по итератору 

O(1) (не совсем честно)

* категория итератора 

Forward


### Устройство контейнеров

#### std::vector

```c++
template <typename T>
class Vector {
 public:
  Vector(size_t n, const T& value = ());

  T& operator[](size_t i) {
    return arr_[i];
  }

  const T& operator[](size_t i) const {
    return arr_[i];
  }

  T& at(size_t i) {
    if (i >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[i];
  }

  const T& at(size_t i) const {
    if (i >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[i];
  }

  size_t size(return size_; )
  size_t capacity(return capacity_; )

  void resize(size_t n, const T& value = T());

  void reserve(size_t n);

 private:
  T* arr_;
  size_t size_;
  size_t capacity_;
}
```

Чтобы лучше прочувствовать разницу между resize и reserve, давайте посмотрим на результат выполнения следующего кода:

```c++
std::vector<int> v;
for (int i = 0; i < 50; ++i) {
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

Еще пример:

```c++
std::vector<int> v;
for (int i = 0; i < 50; ++i) {
  v.pop_back()
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

капасити не уменьшается (даже если вызвать clear)! Для этого в c++11 появился метод shrink_to_fit();

```c++
std::vector<int> v;
for (int i = 0; i < 50; ++i) {
  v.pop_back()
  v.shrink_to_fit();
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

Теперь все ок


Продолжим писать наш вектор. Напишем метод reserve


```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = new T[n];
  for (size_t i = 0; i < size_; ++i) {
    new_arr[i] = arr[i];
  }
  delete[] arr;
  arr = new_arr;
}
```

Какие есть проблемы в этом коде?

1. T* new_arr = new T[n]; может не быть конструктора по умолчанию
2. Мы зачем-то создаем объекты, хотя нас об этом не просили

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new_arr[i] = arr[i];
  }
  delete[] arr;
  arr = new_arr;
  capacity_ = n;
}
```

Уже лучше! Но теперь в строке  new_arr[i] = arr[i]; у нас сегфолт :(

Нам нужно вызвать конструктор T на памяти arr[i]. Как это сделать?

Нам поможет оператор emplacement new

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new(new_arr + i) T(arr[i]);
  }
  delete[] arr;
  arr = new_arr;
  capacity_ = n;
}
```

Теперь у нас сегфолт в строке delete[] arr;


```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new(new_arr + i) T(arr[i]);
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

Теперь давайте напишем push_back

```c++
template <typename T>
void Vector<T>::push_back(const T& value) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(value);
  size_ += 1;
} 
```

Ну можно и pop_back написать:

```c++
template <typename T>
void Vector<T>::pop_back(const T& value) {
  (arr + size_ - 1)->~T();
  size_ -= 1;
} 
```


resize пишется очевидно)

Давайте посмотрим на метод reserve и поймем какие в нем еще проблемы:


```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new(new_arr + i) T(arr[i]);
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

1. Копирование это не очень эффективно, но это научимся решать, когда пройдем мув семантику

2. Небезопасно относительно исключений (если конструктор копирования бросит исключение, то мы не освободим нашу память)

Давайте починим вторую проблему


```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  size_t i = 0;
  try {
    for (i < size_; ++i) {
      new(new_arr + i) T(arr[i]);
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      (new_arr + j)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }
  

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

Чтобы не писать этот блок каждый раз, в стандартной библиотеке существует функция [std::uninitialized_copy](https://en.cppreference.com/w/cpp/memory/uninitialized_copy)

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  try {
    std::uninitialized_copy(arr_, arr_ + size_, new_arr);
  } catch (...) {
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

#### std::vector<bool>

Посмотрим на следующий код:

```c++
template <typename T>
void f() = delete;

std::vector<bool> v(10, false);
v[5] = true;
f(v[5]);
```

На самом деле v[i] это std::_Bit_reference

Посмотрим на примерную реализацию

```c++
template <>
class Vector<bool> {
 public:
  struct BitReference {
    int8_t* cell;
    uint8_t index;

    BitReference& operator=(bool b) {
      if (b) {
        *cell |= (1u << num);
      } else {
        *cell &= ~(1u << num);
      }
    }

    operator bool() const {
      return *cell & (1u << num);
    }
  }

  BitRef operator[](size_t i) {
      return BitReference(arr_ + i / 8, i % 8);
  }
 private:
  int8_t* arr;
  size_t size_;
  size_t capacity_;
  
};
```







