# Лекция 1

## 1. Введение

### 1. Информация про курс

Лектор: Даниил Гагаринов, t.me/yaishenka

[Канал](https://sht.yaishenka.site/cpp_algo_channel) и [чат](https://sht.yaishenka.site/cpp_algo_channel) 

Курс годовой, 30 лекций. Будут задачки + экзамен в конце года

Полезные ссылки и литература:

- [cppreference](https://en.cppreference.com/w/)
- [stackoverflow](https://stackoverflow.com)
- [google](https://google.com)
- [google c++ codestyle]()
- Книга Язык программирования C++, Бьёрн Страуструп
- Книга Effective C++, Скотт Мэйерс
 
 
### 2. Общая информация про язык

Автор C++ Бьёрн Страуструп, 1983

Зачем?

### 3. Codestyle

Он есть и ему надо следовать

### 4. HelloWorld

## 2. Немного про память

### 1. New и delete

Calloc malloc + free

new delete


