# Лекция 13

## rvalue-ссылки и правила их инициализации

Будем смотреть на примере int.

Вспомним сначала про lvalue-ссылки

```c++
int main() {
  int x = 0;
  int& y = x;
}
```

Эта запись означает что y это другое имя для x

Но написать вот так: 

```c++
int main() {
  int& y = 1;
}
```

мы не можем, потому что мы не можем инициализировать не константную lvalue-ссылку с помощью rvalue. А вот константную можем: 

```c++
int main() {
  const int& y = 1;
}
```

С rvalue-ссылками все наоборот: мы не можем инициализировать rvalue-ссылку с помощью lvalue. То есть следующий код не скомпилируется: 

```c++
int main() {
  int x = 0;
  int&& y = x;
}
```

А вот такой уже будет работать:

```c++
int main() {
  int&& y = 0;
}
```

И вот такой тоже:

```c++
int main() {
  int x = 1;
  int&& y = 0;
  y = x;
}
```

Вопрос века: будет ли работать следующий код? 

```c++
int main() {
  int&& x = 0;
  int& z = x;
}
```

<details> 
  <summary>Спойлер</summary>
   Ответ: да, потому что x это lvalue
</details>

А такой?


```c++
int main() {
  int&& x = 0;
  int&& y = x;
}
```

<details> 
  <summary>Спойлер</summary>
   Ответ: нет, потому что x это lvalue
</details>


Чтобы создать rvalue-ссылку на уже существующий объект, нам нужна функция std::move:

```c++
int main() {
  int&& x = 0;
  int&& y = std::move(x);
}
```

Теперь можно разобраться как работает перегрузка конструкторов. Для этого вспомним наш класс string:

```c++
class String {
 public:
  // 1
  String(const String& s) {
    str_ = new char[s.size_]
    memcpy(str_, s.str_, size_);
  }
  // 2
  String(String&& s): str_(s.str_), size_(s.size_) {
    s.str_ = nullptr;
    s.size_ = 0;
  }

 private:
  char* str_ = nullptr;
  size_t size_ = 0;
}
```

Если вызываться от lvalue то вызовется первый конструктор, потому что String&& s нельзя проинициализировать с помощью lvalue. А вот для rvalue вызовется второй конструктор.

## Универсальные ссылки и правила сжатия ссылок

Хотим реализовать функцию std::move. Но есть проблема: мы не умеем писать функции, которые могут принимать и lvalue и rvalue:

```c++
foo(const type& x) // принимает и то и то, но const
foo(type& x) // принимает только lvalue
foo(type&& x) // принимает только rvalue
```

Разработчики стандарта тоже столкнулись с этой проблемой и придумали следующий костыль: 

```c++
template <typename T>
void f(T&& x) {

}
```

Вот в таком контексте x будет являться универсальной ссылкой

Цитата Мейерса: "If a variable or parameter is declared to have type T&& for some deduced type T, that variable or parameter is a universal reference."

Чтобы понять какие у этих ссылок правила вывода, сначала разберемся с правилом сжатия ссылок (reference collapsing)

В c++ не допускается взятие ссылки на ссылку, но такое возникакает при шаблонных подстановках. 

Например:

```c++
template <typename T>
void foo(T x) {
  T& y = x;
}

int main() {
  int x = 0;
  foo<int&>(x);
}
```

При подстановке получится int& & y = x, что запрещено, потому компилятор преобразует это в просто int&. До c++11 такое поведение не было стандартизированно, а вот с появлением rvalue-ссылок правило пришлось ввести. Оно очень простое: одиночный & всегда побеждает. То есть:

1. & и & это &
2. && и & это &
3. & и && это &
4. && и && это &&


Теперь можно понять какие правила вывода работают для универсальных ссылок:

```c++
template <typename SomeType>
void f(SomeType&& x) {}

int main() {
  f(5); // SomeType = int, decltype(x) = int&&
  int y = 5;
  f(y); // SomeType = int&!, decltype(x) = int&;
}
```

Вывод: тип будет lvalue-ссылкой или rvalue-ссылкой в зависимости от типа value которое передали в функцию

Два важных замечания:

***Note*** T&& является универсальной ссылкой, только если T это шаблонный параметр этой функции. (Например в контексте push_back в векторе T&& value не является универсальной ссылкой, так как T это шаблонный параметр класса)

***Note** При выборе перегрузки универсальные ссылки считаются предпочтительней

Теперь можно перейти к реализации std::move

## Реализация std::move

Хотим функцию, которая будет 

Заготовка у нас будет такая: 


```c++
template <typename T>
? move(T&& x) noexcept {

}
```

Давайте определимся с возвращаемым типом. Написать T&& нельзя, потому что нам нужно всегда возвращать rvalue-ссылку, а с учетом правила сжатий ссылок такого не получится. Вспоминаем про type-traits!

```c++
template <typename T>
std::remove_reference_t<T>&& move(T&& x) noexcept {
  return ...
}
```

Теперь нужно понять что нужно написать после слова return. 

Написать просто return x нельзя (пытаемся инициализировать rvalue-ссылку с помощью lvalue). Поэтому придется написать следующее:

```c++
template <typename T>
std::remove_reference_t<T>&& move(T&& x) noexcept {
  return static_cast<std::remove_reference_t<T>&&>(x);
}
```

Итого мы: 

1. Научились делать push_back
2. Научились делать swap
3. Научились передавать временные объекты без копирования

## Когда не надо писать std::move

Допустим у нас есть функция get() которая возвращает временный объект (то есть это rvalue)

Тогда не нужно писать f(std::move(get())), можно просто написать f(get())

## Perfect-forwarding problem и функция std::forward 

Давайте чинить emplace_back нашего вектора 


 Сейчас он выглядит так:

```c++
template <typename... Args>
void emplace_back(const Args&... args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

Очевидно что надо поменять тип аргументов на универсальную ссылку


```c++
template <typename... Args>
void emplace_back(Args&& args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

Теперь тип каждого аргумента в "пачке" будет либо lvalue ref либо rvalue ref (хоть тип args и lvalue)

Написать std::move(args...) нельзя, так как все аргументы мувнуться 


Чтобы решить эту проблему используется функция std::forward


```c++
template <typename... Args>
void emplace_back(Args&& args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(std::forward<Args>(args)...);
  size_ += 1;
}
```

Заметьте, что шаблонный параметр надо указывать явно.

Давайте ее напишем

```c++
template <typename T>
T&& forward(std::remove_reference_t<T>& x) noexcept {
  return static_cast<T&&>(x);
}
```

Давайте разберем случаи:

Первый:

arg = lvalue
Arg = type&
decltype(arg) = type&
T = type&
x = type&
T&& = type&


Второй:

arg = rvalye
Arg = type
decltype(arg) = type&&
T = type
x = type&
T&& = type&&

Теперь мы муваем аргументы, которые можем. А остальные копируем