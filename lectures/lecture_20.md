# Лекция 20

Мы остановились на реализации is_copy_constructible. Теперь давайте реализуем is_nothrow_constructible

## Реализация is_nothrow_constructible

is_nothrow_constructible можно выразить как is_constructible и is_nothrow_constructible. Давайте для начала реализуем это самое "и" 

### Реализация and

Напомню, что std::conditional это специальная метафункция, которая принимает bool и два типа. std::conditional::type равен первому типу, если bool равен true, и второму типу в другом случае.

Также стоит вспомнить что такое true_type. На самом деле true_type это integral_constant\<bool, true\>.

```c++
template<typename...>
struct and;

template<>
struct and<> : public true_type
{ };

template<typename B1>
struct and<B1> : public B1
{ };

template<typename B1, typename B2>
struct and<B1, B2> : public std::conditional<B1::value, B2, B1>::type
{ };


template<typename B1, typename B2, typename B3, typename... Bn>
struct and<B1, B2, B3, Bn...> : public std::conditional<B1::value, and<B2, B3, Bn...>, B1>::type
{ };
```

Устройство and довольно очевидно, поэтому перейдем к is_nt_constructible_impl

### Реализация is_nt_constructible_impl

Идея реализации основана на том, что noexcept работает на этапе компиляции (можете вспомнить про этот оператор в соответствующей лекции). 

```c++
template<typename T, typename... Args>
struct is_nt_constructible_impl 
: public integral_constant<bool, noexcept(T(declval<Args>()...))>
{ };

template<typename T, typename Arg>
struct is_nt_constructible_impl<T, Arg>
: public integral_constant<bool, noexcept(static_cast<T>(declval<Arg>()))>
{ };

template<typename T>
struct is_nt_constructible_impl<T>
: public is_nothrow_default_constructible<T>
{ };
```

***Note*** is_nothrow_default_constructible реализуется очевидно, не будем приводить ее реализацию здесь

Теперь давайте соберем нашу матрешку:

### is_nothrow_constructible

```c++
template<typename T, typename... Args>
struct is_nothrow_constructible
: public and<is_constructible<T, Args...>,
is_nt_constructible_impl<T, Args...>>
{ };
```

Поподставляйте различные сочетания в and и поймите как это работает) 

## move_if_noexcept

Довольно важная метафункция, она нужна нам чтобы гарантировать безопасность относительно исключений (ведь если исключение происходит во время мува, то это невозможно нормально исправить)

```c++
template<typename Tp>
struct move_if_noexcept_cond
: public and<__not_<is_nothrow_move_constructible<Tp>>,
             is_copy_constructible<Tp>>::type 
{ };

template<typename Tp>
constexpr typename
conditional<move_if_noexcept_cond<std::remove_reference_t<Tp>>::value, const std::remove_reference_t<Tp>&, std::remove_reference_t<Tp>&&>::type
move_if_noexcept(std::remove_reference_t<Tp>& x) noexcept
{ return std::move(x); }
```

Вся задумка в возвращаемом значении: если все хорошо то мы вернем T&&, а если нет то const T&

## common_type

Важная метафункция, которая находит LCA в дереве наследования. 

Основная идея: давайте использовать тернарник, ведь он уже умеет находить LCA.

Для начала введем два вспомогательных типа:

```c++
template<typename T>
struct success_type { using type = T; };

struct failure_type { };
```

То есть в success_type::type лежит T, а в failure_type юзинга type нет (это нужно нам для сфинае)

Теперь нам нужна вспомогательная метафункция do_common_type_impl

```c++
struct do_common_type_impl {
  template<typename Tp, typename Up>
  static success_type<typename decay<decltype
                            (true ? std::declval<Tp>() : std::declval<Up>())>::type> S_test(int);

  template<typename, typename>
  static failure_type S_test(...);
};
```

В ней объявлены две перегрузки функции S_test (такой прикол мы уже видели в is_constructible). В первой перегрузке мы как раз пользуемся тернарником, он и помогает там определить общего предка (а если его нет, то сработает сфинае). 

***Note*** std::decay это метафункция которая отбрасывает все модификаторы типа

```c++
template<typename Tp, typename Up>
struct common_type_impl : private do_common_type_impl {
  typedef decltype(S_test<Tp, Up>(0)) type;
};
```

Теперь можно реализовать common_type:

```c++
template <typename ...>
struct common_type;

template<typename Tp>
struct common_type<Tp> { 
  typedef typename decay<Tp>::type type; 
};

template<typename Tp, typename Up>
struct common_type<Tp, Up> : public common_type_impl<Tp, Up>::type
{ };
```

## integer_sequence

Это такой "массив", который можно использовать на этапе компиляции. Мы реализуем только index_sequence (integer_sequence из size_t), потому что это проще) 

С примерами применения можно ознакомиться [тут](https://en.cppreference.com/w/cpp/utility/integer_sequence)

Давайте реализовывать.

```c++
template<size_t... Idx>
struct index_sequence {
 static constexpr size_t size() noexcept { return sizeof...(Idx); }
};
```

Эта структура и будем нашим массивом (а сами данные хранятся в ее шаблонах).

Теперь нам надо реализовать функцию make_index_sequence, которая по числу N будет создавать массив 0,1,...,N-1


Идея такая: давайте брать index_sequence от 0 до N-2 и приклеивать N-1. Давайте напишем функцию push_back:

```c++
template <typename T, size_t N>
struct push_back;

template <size_t N, size_t... Ints> 
struct push_back<index_sequence<Ints...>, N> {
  using type = index_sequence<Ints..., N>;
}
```

Довольно очевидно. Теперь давайте напишем make_index_sequence:

```c++
template <size_t N>
struct make_index_sequence_helper {
  using type = push_back<typename make_index_sequence<N-1>::type, N-1>::type;
}

template<>
struct make_index_sequence_helper<0> {
  using type = index_sequence<>;
}

template <size_t N>
using make_index_sequence = typename make_index_sequence_helper<N>::type;
```

Наконец-то мы можем перейти к базовой рефлексии 

## Basic reflection

Определение из вики:

Рефлексия — формулировка, охватывающая широкий круг явлений и концепций, так или иначе относящихся к обращению разума, духа, души, мышления, сознания, человека (как родового существа или как индивидуума), коллективов на самого себя.

Термин исключительно психологический, но он используется в программировании:

Отражение (рефлексия) — процесс, во время которого программа может отслеживать и модифицировать собственную структуру и поведение во время выполнения.

В c++ рефлексия только зарождается (начиная с 20х плюсов). Однако мы обсудим концепцию, предложенную Антоном Полухиным, который придумал "немного магии для c++14". [Статья на хабре](https://habr.com/ru/post/344206/)

Итак, мы реализуем с вами специальную функцию, которая будет возвращать нам количество полей для POD (plain old data) структур. Нам важно, что такие структуры можно инициализировать с помощью фигурных скобочек:

```c++
struct S {
  int x;
  double d;
}

int main() {
  S s = {1, 2.0};
}
```

Итак, цель Антона была такая: давайте научимся печатать такие структуры в поток вывода. Однако это очень сложная задача, поэтому мы коснемся только ее части (то есть подсчета полей в таких структурах)

Идея следующая: давайте создадим тип "оборотень", то есть тип который можно воспринимать как любой другой типа. Затем будем пробовать инициализировать нашу структуру большим количеством таких типов (привет SFINAE). Когда-нибудь у нас получится и на этом этапе мы поймем сколько полей в нашей структуре. 

Для начала разберемся с нашим "оборотнем":

```c++
struct ubiq {
  template <typename T>
  operator T();
}

template <size_t N>
using ubiq_constructor = ubiq;
```

ubiq_constructor нам нужен, чтобы конструктор структуры стал шаблонным (это пригодится в дальнейшей реализации).

Теперь давайте напишем  detect_fields_count

```c++
template <class T, size_t I0, size_t... I>
auto detect_fields_count(std::index_sequence<I0, I...>) 
  -> decltype(T{ubiq_constructor<I0>{}, ubiq_constructor<I>{}...}, size_t()) {
    return sizeof...(I) + 1;
}

template <class T, size_t... I>
size_t detect_fields_count(index_sequence<I...>) {
  return detect_fields_count<T>(make_index_sequence<sizeof...(I) - 1>{});
}
```

Итак, с помощью index_sequence мы понимаем каким количеством убиков мы пытаемся инициализировать нашу структуру. В первую перегрузку мы попадем, только если при шаблонной подстановке все будет ок, то есть если мы можем инициализировать нашу структуру таким количеством полей. Вторая структура же будет только откусывать по одному убику от нашего пакета

## Compile-time вычисления

### Вычисления с помощью шаблонов

#### Вычисление N-го числа Фибоначчи

Давайте попробуем написать вычисление N-го числа Фибоначчи на шаблонах:

```c++
template <size_t N>
struct Fibonacci {
  static const size_t value = Fibonacci<N-1>::value + Fibonacci<N-2>::value;
}

template <>
struct Fibonacci<0> {
  static const size_t value = 0;
}

template <>
struct Fibonacci<1> {
  static const size_t value = 1;
}
```

#### Проверка простоты числа

Иногда нам может быть важно простое ли число нам передали в шаблон (например при написании класса поля). Мы напишем достаточно тупую проверку за линюю. 

Для начала давайте напишем PrimeHelper

```c++
template <size_t N, size_t D>
struct IsPrimeHelper {
  static const bool value = N % D == 0 ? false : IsPrimeHelper<N, D-1>::value;
};

template <size_t N, size_t D>
struct IsPrimeHelper<N, 1> {
  static const bool value = true;
}
```

Теперь можно написать IsPrime

```c++
template <size_t N>
strict IsPrime {
  static const bool value = IsPrimeHelper<N, N-1>::value;
}

template <>
strict IsPrime<1> {
  static const bool value = false;
}

template <>
strict IsPrime<0> {
  static const bool value = false;
}
```

У этой реализации есть проблема: если мы возьмем достаточно большое число, то мы упадем с ошибкой компиляции (превысим глубину шаблонной рекурсии)

Задача на подумать: реализуйте проверку простоты за корень

### constexpr функции и переменные (since c++11)

Иногда нам нужны константы, известные на этапе компиляции. Например при объявлении std::array

Для таких констант существует ключевое слово constexpr. 

```c++
int main() {
  constexpr int x = 10;
  std::array<int, x> a;
}
```

Но constexpr могут быть не только переменные, но и функции:

```c++
constexpr int factorial(int n) {
  if (n == 0) {
    return 1;
  }

  return n * factorial(n-1);
}

int main() {
  constexpr int x = 3;
  std::array<int, factorial(x)> a;
}
```

Также стоит сказать про if constexpr (since c++17)

```c++
if constexpr (sizeof(int) == 4) {
  std::cout << 1;
}
```

Главное отличие if constexpr от обычного if'а: не true ветки не будут компилироваться. 

***Note*** Очевидно, что внутри if constexpr можно писать только constexpr выражения

Вернемся к constexpr функциям. В них, например, нельзя создавать объекты. То есть такой код не скомпилируется:

```c++
struct S {
  int x;
}

constexpr int f(int n) {
  S s;
  return 0;
}

int main() {
  f(0);
}
```

Но если пометить конструктор как constexpr, то скомпилируется:

```c++
struct S {
  constexpr S() {}
  int x;
}

constexpr int f(int n) {
  S s;
  return 0;
}

int main() {
  f(0);
}
```

В constexpr функциях нельзя бросать исключения. 

При этом throw написать можно:

```c++
constexpr int f(int n) {
  if (n == 0) {
    throw 1;
  }

  return n;
}

int main() {
  f(10);
}
```


***Note*** в c++20 можно делать даже new в compile-time, но это уже совсем другая история...


Еще одно замечание: constexpr функции вычисляются на этапе компиляции, только если их вызов происходит в constexpr выражении













