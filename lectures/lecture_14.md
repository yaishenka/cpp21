# Лекция 14

## move_if_noexcept

Опять обратимся к нашей реализации вектора

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  size_t i = 0;
  try {
    for (i < size_; ++i) {
      new(new_arr + i) T(std::move(arr_[i]));
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      (new_arr + j)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr_ + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr_);
  arr_ = new_arr;

  capacity_ = n;
}
```

Если во время std::move(arr[i]) произойдет исключение (исключение во время вызова конструктора перемещения), то у нас будут проблемы. Поэтому в стандартной библиотеке используется функция std::move_if_noexcept (по названию довольно очевидно что она делает)


## reference qualifiers

Вспомним про const в методах класса. Тогда у нас была проблема с оператором [], и const помогал нам выбрать нужную перегрузку. Так же с ссылками:

```c++
struct S {
  void f() & {
    std::cout << 1;
  }

  void f() && {
    std::cout << 2;
  }
}

int main() {
  S s;
  s.f()
  std::move(s).f();
}
```

Пример применения: 

Есть у нас класс BigInteger. Есть для него оператор +. Мы хотим чтобы выражения вида a + b = 5 не работали. До C++ 11 это решалось тем, что operator+ возвращал const BigInteger. Начиная с c++11 нужно просто оператор присваивания пометить rvalue qualifier:

```c++
struct BigInteger {
  BigInteger& opeartor=(const BigInteger&) &;
}
```

***Note*** если вообще не ставить qualifier то метод будет одинаково работать для обоих видов value.

## Перегрузка new и delete. Аллокаторы.

### Перегрузка стандартных new и delete

Давайте посмотрим на такой пример:

```c++
struct S {
  int x = 0;
}

int main() {
  S* ptr = new S(); // 1
}
```

Как мы уже знаем в строке 1 должна выделиться память и вызваться конструктор S (в данном случае конструктор по умолчанию)

Перегрузить можно только само выделение памяти. Давайте это сделаем

```c++
void* operator new(size_t n) {
  std::cout << "We are in new";
  return malloc(n);
}

void operator delete(void* p) noexcept {
  std::cout << "We are in delete";
  free(p);
}

struct S {
  int x = 0;
}

int main() {
  S* ptr = new S(); // 1
  delete s;
}
```

Таким образом мы смогли перезгрузить глобальный оператор new и delete. Но это не все операторы new и delete, потому что есть еще их версии для массивов

```c++
void* operator new[](size_t n) {
  std::cout << "We are in new array";
  return malloc(n);
}

void operator delete[](void* p) noexcept {
  std::cout << "We are in delete";
  free(p);
}
```

Давайте убедимся что наши перегрузки действительно работают

```c++
void* operator new(size_t n) {
  std::cout << "We are in new array";
  return malloc(n);
}

void operator delete(void* p) noexcept {
  std::cout << "We are in delete";
  free(p);
}

int main() {
  std::vector<int> v(10);
  v[0] = 10;
}
```

***Note*** Вектор использует new без \[\], позже поймем почему

То есть перезгрузка работает. Теперь давайте поймем, почему такая перегрузка плохая 

Оператор new умеет кидать исключение, если он не смог выделить память, поэтому:

```c++
void* operator new(size_t n) {
  std::cout << "We are in new";

  void* ptr = malloc(n);

  if (ptr == nullptr) {
    throw std::bad_alloc();
  }

  return ptr;
}

void operator delete(void* p) {
  std::cout << "We are in delete";
  free(p);
}
```

На самом деле стандартный оператор new в случае провала сначала пытается вызвать функцию std::new_handler, и только в случае ее провала бросает исключение.

Еще одно важное замечание: если вы запросите 0 байт у new, то он выделит 1 байт

## Другие формы оператора new/delete

### Перегрузка оператора new для конкретного типа

```c++
struct S {
  static void* operator new(size_t n) {
    std::cout << "We are in new for S";

    void* ptr = malloc(n);

    if (ptr == nullptr) {
      throw std::bad_alloc();
    }

    return ptr;
  }

  static void operator delete(void* p) {
    std::cout << "We are in delete for struct S";
    free(p);
  }
}
```

***Note*** Статик будет по умолчанию, но лучше не забывать писать

***Note*** При выборе какой оператор выбрать для типа, предпочтение будет отдано оператору, который объявлен внутри класса


### placament-new

Давайте составим вот такой искуственный пример:

```c++
struct S {
 private:
  S() {
    std::cout << "Private construct S\n";
  }

 public:
  static void* operator new(size_t n) {
    std::cout << "We are in new for S";

    void* ptr = malloc(n);

    if (ptr == nullptr) {
      throw std::bad_alloc();
    }

    return ptr;
  }

  static void operator delete(void* p) {
    std::cout << "We are in delete for struct S";
    free(p);
  }
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Это не сработает, потому что если для типа определен кастомный оператор new/delete то placement new сгенерирован не будем
Давайте удалим перегрузку

```c++
struct S {
 private:
  S() {
    std::cout << "Private construct S\n";
  }

 public:
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Теперь CE потому что конструктор приватный. Давайте перегрузим placement new и сделаем его другом


```c++
struct S {
  friend void* operator new(size_t, S*);
 private:
  S() {
    std::cout << "Private construct S\n";
  }

 public:
}

void* operator new(size_t, S* p) {
  return p;
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Перегрузка именно такая, потому что часть с вызовом конструкторов мы перегружать не можем. А выделять память в placement new уже не нужно.

Это опять не работает, потому что функция operator new конструктор не вызывает



```c++
struct S {
  S() {
    std::cout << "Private construct S\n";
  }
}

void* operator new(size_t, S* p) {
  return p;
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Кастомный оператор delete при этом компилятор не вызывает, кроме случая, когда в операторе new произойдет исключение

Пример:

```c++
struct S {
  S() {
    throw 1;
  }
}

void* operator new(size_t n, double d) {
  std::cout << "custom new with d: " << d << std::endl;
  return malloc(n);
}

void operator delete(void* p, double d) {
  std::cout << "custom delete with d: " << d << std::endl;
  return free(p);
}

int main() {
  try {
    S* p = new(3.14) S();
  } catch (int) {
    
  }
}
```













