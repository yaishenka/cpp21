# Лекция 15

## Идея аллокаторов

На данный момент мы знаем, что в c++ память выделяется с помощью оператора new, который в свою очередь обращается к malloc.

То есть new это абстракция над выделением памяти в Си. Но это все еще довольно низкоуровневая вещь, плюс с ней не очень удобно работать. Например: хотим для типа MyType чтобы std::vector выделял память одним способом, а std::set другим. С помощью перегрузок оператора new этого не добиться

Поэтому в c++ появилась еще более высокоуровневая абстракция над выделением памяти под названием аллокатор. 
Аллокатор это способ переопределить выделение памяти до момента обращения к оператору new. 

## std::allocator

Давайте посмотрим на реализацию стандартного аллокатора, который используется по умолчанию во всех контейнерах. Он очевидно должен просто обращаться к new/delete 

```c++
template <typename T>
struct Allocator {
  void* allocate(size_t n) {
    return ::operator new(n * sizeof(T));
  }

  void deallocate(T* ptr, size_t) {
    ::operator delete(ptr);
  }

  template <typename... Args>
  void construct(T* ptr, Args&&...) {
    new(ptr) T(std::forward(args)...);
  }

  void destroy(T* ptr) noexcept {
    ptr->~T();
  }
}
```

Конечно в нем еще есть куча using'ов, но это уже не так интересно. 

Как можно заметить аллокатор (как и new) разделяет создание объекта и выделение памяти, правда в случае аллокатора часть с вызовом конструктора можно перегрузить. 

## Пример нестандартного аллокатора: PoolAllocator

В чем идея: давайте в конструкторе аллокатора запросим сразу много памяти. 
При выделении памяти будем отдавать кусочек, а при вызове deallocate делать ничего не будем 


```c++
template <typename T>
class PoolAllocator {
public:
  PoolAllocator() {
    pool_ = ::operator new(pool_size);
  }

  ~PoolAllocator() {
    ::operator delete(pool_);
  }

  void* allocate(size_t n) {
    size_t bytes = n * sizeof(T);
    auto memory_to_return = pool_;
    pool_ += bytes;
    return memory_to_return;
  }

  void deallocate(T* ptr, size_t) {
    return;
  }

  template <typename... Args>
  void construct(T* ptr, Args&&...) {
    new(ptr) T(std::forward(args)...);
  }

  void destroy(T* ptr) noexcept {
    ptr->~T();
  }

private:
  const size_t pool_size = 1000000;
  uint8_t* pool_;
}
```

А еще можно в аллокаторе выделить память на стеке.

## allocator_traits

Большинство методов у всех аллокаторов будут одинаковые (например construct и destroy) а еще внутри есть куча using, поэтому в c++ была добавлена специальная обертка allocator_traits

allocator_traits это структура, которая шаблонным параметром принимает класс вашего аллокатора

Почти все методы и внутренние юзинги работают по прицнипу "взять у аллокатора если есть, если нет сгенерировать автоматически"

Лучше всего посмотреть на описание и список методов [тут](https://en.cppreference.com/w/cpp/memory/allocator_traits)


## Правильная реализация контейнеров

Давайте вспомним нашу реализацию вектора. Теперь у нее появляется новый шаблонный параметр Alloc

```c++
template <typename T, typename Alloc = std::allocator<T>>
class Vector {
 public:
  Vector(size_t n, const T& value = (), const Alloc& allocator = Alloc());

  T& operator[](size_t i) {
    return arr_[i];
  }

  const T& operator[](size_t i) const {
    return arr_[i];
  }

  T& at(size_t i) {
    if (i >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[i];
  }

  const T& at(size_t i) const {
    if (i >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[i];
  }

  size_t size(return size_; )
  size_t capacity(return capacity_; )

  void resize(size_t n, const T& value = T());

  void reserve(size_t n);

 private:
  using AllocTraits = std::allocator_traits<Alloc>; // это не обязательно, но так будет удобней

  T* arr_;
  size_t size_;
  size_t capacity_;
  Alloc alloc_; // Сохраняем к себе!
}
```

А теперь поймем как правильно обращаться к аллокатору на примере метода reserve:

***

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  // T* new_arr = new T[n]; // На неуд
  // T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]); На удос
  // T* new_arr = alloc_.allocate(n); На хор
  T* new_arr = AllocTraits::allocate(alloc_, n);  // На отл

  size_t i = 0;
  try {
    for (;i < size_; ++i) {
      AllocTraits::construct(alloc_, new_arr + i, std::move(arr_[i]));
    } 
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      AllocTraits::destroy(alloc_, new_arr + j);
    }
    AllocTraits::dallocate(alloc_, n);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    AllocTraits::destroy(alloc_, arr_ + i);
  }

  AllocTraits:deallocate(alloc_, arr_, capacity_)

  arr_ = new_arr;
  capacity_ = n;
}
```

Тоже самое надо проделать во всех остальных методах вектора


## Rebinding allocators 

Проблема: есть класс list. Он внутри аллоцирует не T а Node\<T\>, а аллокатор у него Allocator\<T\>

Раньше внутри аллокатора был шаблон rebind, который выглядел так: 

```c++
class Allocator {
  template <typename U>
  struct rebind {using other = Allocator<U>; }
}
```

Тогда внутри своего класса (в данном случае List) вы обращались бы к Alloc::rebind\<Node\<T\>::other

Но 

1. Это неудобно
2. Это опять одинаково у всех аллокаторов (почти)

Поэтому функцию rebind перенесли в std::allocator_traits

TODO(yaishenka): дописать как пользоваться rebind

## Копирование и конструирование аллокаторов друг от друга

TODO(yaishenka): дописать общую идею

### select_on_container_copy_construction

TODO(yaishenka): дописать что делать при конструировании 

### propagate_on_container_copy_assignemnt

TODO(yaishenka): дописать что делать при копировании/перемещении
