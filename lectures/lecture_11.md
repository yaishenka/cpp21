# Лекция 11

## Продолжаем говорить про контейнеры 

### Stack, queue, priority_queue

По сути это не отдельные контейнеры, это адаптеры над контейнерами. Давайте посмотрим на стек:

```c++
template <typename T, typename Container = std::deque<T>>
class Stack {
 public:
  void push(const T& value) {
    container_.push_back(value);
  }

  void pop() {
    container_.pop_back();
  }

  T& top() {
    return container_.back();
  }
 private:
  Container container_;
}
```

<details> 
  <summary>Почему pop ничего не возвращает?</summary>
   Ответ: все ради эффективности, если бы pop что-то возвращал, то он был бы обязан копировать, а это проигрыш
</details>

Аналогично устроены queue и priority_queue

### list

```c++
template <typename T>
class List {
 public:
  

 private:
  struct Node {
    T value;
    Node* prev;
    Node* next;
  }

  Node* head;
}
```

Интересные факты:

1. std::sort не работает для list, так как не RA итератор. Поэтому у листа есть встроенный метод sort (использует merge)

2. Есть метод merge

3. Есть метод сплайс

### map

Хранит пары ключ, значение. При этом ключи упорядочены 

Итераторы константные

Реализация почти везде сделана на красно-черном дереве

Формат ноды:

```c++
struct Node {
  std::pair<const Key, Value> data;
  Node* prev;
  Node* left;
  Node* right;
}
```

***Note*** оператор [] создает элемент в мапке, если его не было (если не хотим создавать, используем at)

### unordered_map (хэш-таблица)

Хранит неупорядоченные пары ключ значение. 

### set unordered_set

Тоже самое что и unordered_map


## Инвалидация итераторов

Давайте рассмотрим проблему на примере вектора. 

```c++
std::vector<int> v;
v.push_back(1);

std::vector<int>::iterator = v.begin();
int* first_ptr = &v[0];
int& first_ref = v[0];

for (int i = 0; i < 100; ++i) {
  v.push_back(i);
}
```

После такого цикла вектор поменяет свой размер и сделает реаллокацию. Тогда обращение по итератору/указателю/ссылке будет UB. Говорят что push_back инвалидирует итераторы/указатели/ссылки.

Давайте поймем какие операции над стандартыми контейнерами что делают.

![invalidation_rules](/lectures/images/invalidation_rules.png)




















